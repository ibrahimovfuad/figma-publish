export const FIGMA_BASE_URL = "https://www.figma.com/api/";

export const HEADERS = {
  "Content-Type": "application/json",
  "x-csrf-bypass": "yes",
  Cookie: "",
};
