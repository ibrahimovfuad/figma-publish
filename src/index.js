#!/usr/bin/env node
import { FigmaPublish } from "./FigmaPublish";
import axios from "axios";
import { authenticator } from "otplib";
import { config } from "dotenv";

config();

const { FIGMA_EMAIL, FIGMA_PASSWORD, FIGMA_USERNAME, FIGMA_SECRET } =
  process.env;

const figmaPublish = new FigmaPublish({
  api: axios,
  otpauth: authenticator,
  email: FIGMA_EMAIL,
  password: FIGMA_PASSWORD,
  username: FIGMA_USERNAME,
  secret: FIGMA_SECRET,
});

(async () => {
  await figmaPublish.startPublish();
})();
