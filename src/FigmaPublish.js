import fs from "fs-extra";
import { getArguments, parseCookies } from "./utils";
import { FIGMA_BASE_URL, HEADERS } from "./constants";

export class FigmaPublish {
  constructor({ api, otpauth, email, password, username, secret }) {
    this.args = getArguments(process.argv);
    this.api = api;
    this.otpauth = otpauth;
    this.email = email;
    this.password = password;
    this.username = username;
    this.secret = secret;
    this.headers = HEADERS;
  }

  #setCookies = (cookie) => {
    this.headers.Cookie = cookie;
  };

  #getToken = () => {
    return this.otpauth.generate(this.secret);
  };

  async login() {
    const token = this.#getToken();
    const data = {
      email: this.email,
      password: this.password,
      password_retype: this.password,
      username: this.username,
      totp_key: token,
    };

    const res = await this.api({
      method: "POST",
      url: `${FIGMA_BASE_URL}session/login`,
      headers: this.headers,
      data,
    });

    this.#setCookies(parseCookies(res.headers["set-cookie"]));

    console.info("Logged in successfully");
  }

  async upload(url, data) {
    return await this.api({
      method: "PUT",
      url,
      headers: this.headers,
      data,
    });
  }

  uploadIcon = (url) => {
    return this.upload(url, fs.readFileSync(this.args.icon)).then(() =>
      console.info("Icon uploaded successfully")
    );
  };

  uploadCode = (url) => {
    return this.upload(url, fs.readFileSync(this.args.code)).then(() =>
      console.info("Code uploaded successfully")
    );
  };

  uploadCover = (url) => {
    return this.upload(url, fs.readFileSync(this.args.cover)).then(() =>
      console.info("Cover uploaded successfully")
    );
  };

  getManifest() {
    return fs.readJsonSync(this.args.manifest);
  }

  hasRequiredFiles() {
    return (
      fs.existsSync(this.args.manifest) &&
      fs.existsSync(this.args.icon) &&
      fs.existsSync(this.args.cover) &&
      fs.existsSync(this.args.code)
    );
  }

  async publishInfo(id, versionId, signature) {
    await this.api({
      method: "PUT",
      url: `${FIGMA_BASE_URL}plugins/${id}/versions/${versionId}`,
      headers: this.headers,
      data: JSON.stringify({
        icon_uploaded: true,
        cover_image_uploaded: true,
        code_uploaded: true,
        snapshot_uploaded: false,
        comments_setting: "all_disabled",
        agreed_to_tos: true,
        signature: signature,
      }),
    });
  }

  async uploadManifest(manifest) {
    const data = JSON.stringify({
      manifest,
      release_notes: "",
      name: manifest.name,
      description: "<p>test publish plugin</p>", //@todo dynamic description
    });

    const res = await this.api({
      method: "POST",
      url: `${FIGMA_BASE_URL}plugins/${manifest.id}/upload`,
      headers: this.headers,
      data,
    });

    console.info("Manifest uploaded successfully");

    return {
      iconUrl: res.data.meta.icon_upload_url,
      codeUrl: res.data.meta.code_upload_url,
      coverUrl: res.data.meta.cover_image_upload_url,
      versionId: res.data.meta.version_id,
      signature: res.data.meta.signature,
    };
  }

  async startUpload() {
    const manifest = this.getManifest();

    const { iconUrl, coverUrl, codeUrl, versionId, signature } =
      await this.uploadManifest(manifest);

    await Promise.all([
      this.uploadIcon(iconUrl),
      this.uploadCover(coverUrl),
      this.uploadCode(codeUrl),
    ]);

    await this.publishInfo(manifest.id, versionId, signature);

    console.info("Published successfully");
  }

  async startPublish() {
    try {
      if (!this.hasRequiredFiles()) {
        throw new Error(
          "Please check all required files are exist: manifest.json, code.js, icon.png, cover.png"
        );
      }

      await this.login();
      await this.startUpload();
    } catch (err) {
      console.error(err.message);
    }
  }
}
