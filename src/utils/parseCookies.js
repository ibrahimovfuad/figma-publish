export const parseCookies = (cookies) => {
  return cookies.reduce((acc, item) => {
    const cookie = item.split(";")[0];
    acc += cookie + ";";
    return acc;
  }, "");
};
