const defaults = {
  manifest: "./manifest.json",
  icon: "./icon.png",
  code: "./code.js",
  cover: "./cover.png",
};

const mergeArgs = (args) =>
  Object.keys(defaults).reduce((acc, key) => {
    acc[key] = args[key] || defaults[key];
    return acc;
  }, {});

const parseArguments = (args) =>
  args.reduce((acc, item) => {
    const parsedItem = item.split("=");
    const key = parsedItem[0].replace("--", "");
    acc[key] = parsedItem[1];

    return acc;
  }, {});

export const getArguments = (args) => {
  const argsArray = args.slice(2);

  if (!argsArray.length) {
    return defaults;
  }

  return mergeArgs(parseArguments(argsArray));
};
