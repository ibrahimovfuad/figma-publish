## About

figma-publish tool allows publishing plugins from pipeline or terminal and etc

## How to use
#### 1. Variables
This tool need credentials from figma account to publish plugin.
Also, `FIGMA_SECRET` is a secret code which is needed for one-time password generation for 2FA.

    - FIGMA_EMAIL
    - FIGMA_PASSWORD
    - FIGMA_USERNAME
    - FIGMA_SECRET

### 2. Description

Run `npx figma-publish` or if you need to pass custom path to files just provide flags as described below

`npx figma-publish --manifest=<file path> --code=<file path> --cover=<file path> --icon=<file path>`

### 3. Flags

|  Flag  |  Description |  Default  |
|  ---   |  ---   |  ---   |
| `--manifest` |  path to manifest.json      |  `./manifest.json` |
| `--code`     |  path to code file          |  `./code.js`       |
| `--cover`    |  path to cover image file   |  `./cover.json`    |
| `--icon`     |  path to icon image file    |  `./icon.json`     |
